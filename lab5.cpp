#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include "stdlib.h"
#include "string.h"

using namespace std;

enum Suit { SPADES=0, HEARTS=1, DIAMONDS=2, CLUBS=3 };

typedef struct Card {
  Suit suit;
  int value;
} Card;

string get_suit_code(Card& c);
string get_card_name(Card& c);
bool suit_order(const Card& lhs, const Card& rhs);
int myrandom (int i) { return std::rand()%i;}


int main(int argc, char const *argv[]) {
  // IMPLEMENT as instructed below
  /*This is to seed the random generator */
  srand(unsigned (time(0)));

  /*Create a deck of cards of size 52 (hint this should be an array) and
   *initialize the deck*/
   Card cardArray[52];
   Card deck[5];
   //11 = jack, 12= queen, 13 = king , 14 = ace

  /*After the deck is created and initialzed we call random_shuffle() see the
   *notes to determine the parameters to pass in.*/

   for(int i = 0; i < 52; i++)
   {
     //suit 1
     if(i <= 12)
     {
       int k = 0;
       k = i + 2;
       cardArray[i].suit = SPADES;
       cardArray[i].value = k++;
          //sets the ace card value
          if(i == 12)
          {
            cardArray[i].suit = SPADES;
            cardArray[i].value = 14;
          }
     }

     //suit 2
     else if(i <= 25)
     {
       int j = 0;
       j = (i - 13) + 2;
       cardArray[i].suit = HEARTS;
       cardArray[i].value = j;
          //sets the ace card value
          if(i == 25)
          {
            cardArray[i].suit = HEARTS;
            cardArray[i].value = 14;
          }
     }
     //suit 3
     else if(i <= 38)
     {
       int l = 0;
       l = (i - 26) + 2;
       cardArray[i].suit = DIAMONDS;
       cardArray[i].value = l++;
          //sets the ace card value
          if(i == 51)
          {
            cardArray[i].suit = DIAMONDS;
            cardArray[i].value = 14;
          }
     }
     //suit 4
     else
     {
       int h = 0;
       h = (i - 39) + 2;
       cardArray[i].suit = CLUBS;
       cardArray[i].value = h++;
          //sets the ace card value
          if(i == 51)
          {
            cardArray[i].suit = CLUBS;
            cardArray[i].value = 14;
          }
     }

     }

   random_shuffle(&cardArray[0], &cardArray[52], myrandom);

   // puts the first 5 values of the card array into a deck
   for (int i = 0; i < 5; i++)
   {

       deck[i] = cardArray[i];

   }
   /*Build a hand of 5 cards from the first five cards of the deck created
    *above*/
    //builds the hand of cards here
    Card handofCards[5] = {deck[0], deck[1], deck[2], deck[3], deck[4]};

    /*Sort the cards.  Links to how to call this function is in the specs
     *provided*/
     //cards sorted here
    sort(&handofCards[0], &handofCards[5], suit_order);

    /*Now print the hand below. You will use the functions get_card_name and
     *get_suit_code */

     //prints out the final statements here
     for(int i = 0; i < 5; i++)
     {
       if(handofCards[i].value < 11)
       {
         cout << right << setw(10) << handofCards[i].value << " of ";

       }
       else
       {

         cout << right << setw(10) << get_card_name(handofCards[i]) << " of ";
       }

       cout << get_suit_code(handofCards[i]) << endl;

     }

  return 0;
}

/*This function will be passed to the sort funtion. Hints on how to implement
* this is in the specifications document.*/

bool suit_order(const Card& lhs, const Card& rhs)
{
  // IMPLEMENT a bunch of if elses here
  if (lhs.suit < rhs.suit)
  {
    return true;
  }
  //here if the suits are the same then compare the values
  else if (lhs.suit == rhs.suit)
  {
    if(lhs.value < rhs.value)
    {
      return true;
    }
    else
    {
      return false;
    }
  }
  else
  {
    return false;
  }
}

string get_suit_code(Card& c)
{
  switch (c.suit)
  {
    case SPADES:    return "\u2660";
    case HEARTS:    return "\u2661";
    case DIAMONDS:  return "\u2662";
    case CLUBS:     return "\u2663";
    default:        return "";
  }
}

string get_card_name(Card& c) {
  // IMPLEMENT
  // might need this to_string()

  //string initialize
  string cardName = "test";

  if(c.value == 11)
  {
  return cardName = "Jack";
  }
  else if(c.value == 12)
  {
    return cardName = "Queen";
  }
  else if(c.value == 13)
  {
    return cardName = "King";
  }
  else if(c.value == 14)
  {
    return cardName = "Ace";
  }
  else
  {

  }
  //returns the cardname to function
  return cardName;
}
